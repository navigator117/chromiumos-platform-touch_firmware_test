# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from collections import namedtuple

MtFinger = namedtuple('MtFinger', ['tid', 'syn_time', 'x', 'y', 'pressure'])
MtSnapshot = namedtuple('MtSnapshot', ['syn_time', 'button_pressed',
                                       'fingers'])

class MtStateMachine(object):
  """ This is an abstract base class that defines the interface of a multitouch
  state machine.  This class should never be instantiated directly, but rather
  any actual state machines should derive from this class to guarantee that it
  conforms to the same interfaces.
  """
  def add_event(self, event):
    raise NotImplementedError('Error: add_event() not implemented.')

  def get_current_snapshot(self, request_data_ready=True):
    raise NotImplementedError('Error: get_current_snapshot() not implemented.')


class MtaStateMachine(MtStateMachine):
  """ The state machine for MTA events

  This class accepts MtEvents via the add_event() member function and maintains
  the state that an MTA system would.  MTA is slightly different than MTB, but
  uses similar events.  Without delta compression, MTA can get rid of the
  concept of slots, since all the finger data is reported before each SYN_REPORT
  and the fingers themselves are separated byt SYN_MT_REPORTS instead of a
  new SLOT.

  On all the MTA devices I have tested, they use only TOUCH_MAJOR instead of
  ABS_MT_PRESSURE, so the values are substituted here as they serve the same
  purpose.  If this proves to be an issue, it can be addressed later.

  Note that because our system expects new fingers to have new TIDs and in MTA
  this is not the case (they reuse TIDs all the time) the MtaStateMachine
  keeps track of which TIDs have already been used and remaps them when
  it detects a repeated TID to an unused value to maintain transparency
  between MTB and MTA.
  """
  def __init__(self):
    # This dictionary maps the raw (non-unique) tids reported in the events to
    # new (unique) tids so we can easily differentiate any two fingers even if
    # there is a gap of time inbetween them.
    self.tid_mappings = {}
    self.next_unique_tid = 0

    self.button_pressed = False
    self.last_snapshot = None

    self._clear()

  def _clear(self):
    # Clear out the state for the current fingers
    self.fingers_data = []
    self.tid = self.x = self.y = self.pressure = self.touch_major = None

  def add_event(self, event):
    # If this is the end of the information for all the fingers for this report
    if event.is_SYN_REPORT():
      # First build up the snapshot
      fingers = []
      for tid, x, y, touch_major in self.fingers_data:
        mapped_tid = self.tid_mappings[tid]
        fingers.append(MtFinger(mapped_tid, event.timestamp, x, y, touch_major))
      self.last_snapshot = MtSnapshot(event.timestamp, self.button_pressed,
                                      fingers)

      # Delete the tid mappings for any leaving fingers
      # Find all the raw tids that have currently been reported
      raw_tids_used = set([raw_tid for raw_tid, _, _, _ in self.fingers_data])
      # Detect any mappings that are for fingers no longer on the pad
      leaving_raw_tids = [raw_tid for raw_tid in self.tid_mappings
                          if raw_tid not in raw_tids_used]
      # Remove those out-dated mappings from the mapping dictionary
      for raw_tid in leaving_raw_tids:
        del self.tid_mappings[raw_tid]

      # Then clear out the fingers_data since this starts a new snapshot
      self._clear()

    # If this is the end of a finger's information stash all the data about it
    # until the end of the data for all the fingers
    elif event.is_SYN_MT_REPORT():
      if all([v is not None for v in
              (self.tid, self.x, self.y, self.touch_major)]):
        self.fingers_data.append((self.tid, self.x, self.y, self.touch_major))

    # Otherwise, check what information it's giving us and store it
    elif event.is_ABS_MT_TRACKING_ID():
      self.tid = event.value
      if self.tid not in self.tid_mappings:
        self.tid_mappings[self.tid] = self.next_unique_tid
        self.next_unique_tid += 1
    elif event.is_ABS_MT_POSITION_X():
      self.x = event.value
    elif event.is_ABS_MT_POSITION_Y():
      self.y = event.value
    elif event.is_ABS_MT_PRESSURE():
      self.pressure = event.value
    elif event.is_ABS_MT_PRESSURE():
      self.pressure = event.value
    elif event.is_ABS_MT_TOUCH_MAJOR():
      self.touch_major = event.value
    elif event.is_BTN_LEFT():
      self.button_pressed = (event.value == 1)

  def get_current_snapshot(self, request_data_ready=True):
    return self.last_snapshot


class MtbStateMachine(MtStateMachine):
  """ The state machine for MTB events.

  It traces the slots, tracking IDs, x coordinates, y coordinates, etc. If
  these values are not changed explicitly, the values are kept across events.

  Note that the kernel driver only reports what is changed. Due to its
  internal state machine, it is possible that either x or y in
  self.point[tid] is None initially even though the instance has been created.
  """
  def __init__(self):
    # Set the default slot to 0 as it may not be displayed in the MT events
    #
    # Some abnormal event files may not display the tracking ID in the
    # beginning. To handle this situation, we need to initialize
    # the following variables:  slot_to_tid, point
    #
    # As an example, refer to the following event file which is one of
    # the golden samples with this problem.
    #   tests/data/stationary_finger_shift_with_2nd_finger_tap.dat
    self.tid = None
    self.slot = 0
    self.slot_to_tid = {self.slot: self.tid}
    self.x = {self.tid: None}
    self.y = {self.tid: None}
    self.pressure = {self.tid: None}
    self.syn_time = None
    self.new_tid = False
    self.number_fingers = 0
    self.leaving_slots = []
    self.button_pressed = False

  def _del_leaving_slots(self):
    """ Delete the slots and tracking IDs for any leaving fingers """
    for slot in self.leaving_slots:
      del self.slot_to_tid[slot]
      self.number_fingers -= 1
    self.leaving_slots = []

  def _add_new_tracking_id(self, tid):
    """ Add a new tracking ID and slot of an arriving finger """
    self.tid = tid
    self.slot_to_tid[self.slot] = self.tid
    self.new_tid = True
    self.x[self.tid] = None
    self.y[self.tid] = None
    self.pressure[self.tid] = None
    self.number_fingers += 1

  def add_event(self, event):
    """ Update the internal states with the input MtEvent  """
    self.new_tid = False

    # Switch the slot.
    if event.is_ABS_MT_SLOT():
      self.slot = event.value

    # Get a new tracking ID.
    elif event.is_new_contact():
      self._add_new_tracking_id(event.value)

    # A slot is leaving.
    # Do not delete this slot until this last packet is reported.
    elif event.is_finger_leaving():
      self.leaving_slots.append(self.slot)

    # The physical click button is not associated with any finger/slot so
    # it should also be handled independently here but updating the state
    elif event.is_BTN_LEFT():
      self.button_pressed = (event.value == 1)

    else:
      # Gracefully handle the case where we weren't given a tracking id
      # by using a default value for these mystery fingers
      if (not self.slot in self.slot_to_tid and event.is_finger_data()):
        self._add_new_tracking_id(999999)

      # Update x value.
      if event.is_ABS_MT_POSITION_X():
        self.x[self.slot_to_tid[self.slot]] = event.value

      # Update y value.
      elif event.is_ABS_MT_POSITION_Y():
        self.y[self.slot_to_tid[self.slot]] = event.value

      # Update z value (pressure)
      elif event.is_ABS_MT_PRESSURE():
        self.pressure[self.slot_to_tid[self.slot]] = event.value

      # Use the SYN_REPORT time as the packet time
      elif event.is_SYN_REPORT():
        self.syn_time = event.timestamp

  def get_current_snapshot(self, request_data_ready=True):
    """Get current packet's data including x, y, pressure, and
    the syn_time for all tids.

    @param request_data_ready: if set to true, it will not output
            current_tid_data until all data including x, y, pressure,
            syn_time, etc. in the packet have been assigned.
    """
    current_fingers = []
    for slot, tid in self.slot_to_tid.items():
      x = self.x.get(tid)
      y = self.y.get(tid)
      pressure = self.pressure.get(tid)
      # Check if all attributes are non-None values.
      # Note: we cannot use
      #           all([all(point.value()), pressure, self.syn_time])
      #       E.g., for a point = (0, 300), it will return False
      #       which is not what we want. We want it to return False
      #       only when there are None values.
      data_ready = all(map(lambda e: e is not None,
                       [x, y, pressure, self.syn_time]))

      if (not request_data_ready) or data_ready:
        finger = MtFinger(tid, self.syn_time, x, y, pressure)
        current_fingers.append(finger)

    self._del_leaving_slots()

    current_snapshot = MtSnapshot(self.syn_time, self.button_pressed,
                                   current_fingers)
    return current_snapshot
